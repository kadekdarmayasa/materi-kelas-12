public class Overload_Constructor {
  int nilai1, nila2;

  public Overload_Constructor(int bil1) {
    nilai1 = bil1;
  }   

  public Overload_Constructor(int bil1, int bil2) {
    nilai1 = bil1 + bil2;
  }

  public String grade() {
    return nilai1 >= 50 ? "Bagus" : "Jelek";
  }

  public void cetak() {
    System.out.println("Grade nilainya = " + grade());
  }

  public static void main(String[] args) {
    Overload_Constructor hasil = new Overload_Constructor(45);
    hasil.cetak();
    Overload_Constructor hasil2 = new Overload_Constructor(45, 35);
    hasil2.cetak();
  }
}
