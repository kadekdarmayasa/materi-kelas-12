public class Constructor_1 {
  int nilaiAkhir;
  public Constructor_1(int nilai_akhir) {
    nilaiAkhir = nilai_akhir;
  }

  public String grade() {
    return nilaiAkhir >= 50 ? "Bagus" : "Jelek";
  }

  public void cetak() {
    System.out.println("Grade nilainya = " + grade());
  }

  public static void main(String[] args) {
    Constructor_1 hasil = new Constructor_1(67);
    hasil.cetak();
  }
}
