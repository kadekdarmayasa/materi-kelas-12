import java.io.File;
import java.io.IOException;
import java.util.Scanner;

class file_baru {
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.println("Membuat file dengan Java");
    System.out.print("Masukkan nama file: ");
    String namafile = in.nextLine();
    System.out.print("Masukkan tipe ekstensi untuk file " + namafile + ":");
    String eksfile = in.nextLine();
    try {
      String path = "./" + namafile + "." + eksfile;
      File f = new File(path);
      f.createNewFile();
      System.out.print("Pembuatan file berhasil");
    } catch(IOException ex) {
      System.out.print("Pembuatan file gagal");
    }
    in.close();
  }
}
