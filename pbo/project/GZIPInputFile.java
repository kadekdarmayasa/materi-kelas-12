import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class GZIPInputFile {
  public static void main(String[] args) {
    GZIPInputFile zipObj = new GZIPInputFile();
    zipObj.gunzipIt("tes.gz", "hasil.txt");
    zipObj.gunzipIt("tes.zip", "2");
  }

  public void gunzipIt(String INPUT_GZIP_FILE, String OUTPUT_FILE) {
    byte[] buffer = new byte[1024];
    try {
      GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(INPUT_GZIP_FILE));
      FileOutputStream out = new FileOutputStream(OUTPUT_FILE);
      int len;
      while ((len = gzis.read(buffer)) > 0) {
        out.write(buffer, 0, len);
      }
      gzis.close();
      out.close();
      System.out.println("Done");
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
