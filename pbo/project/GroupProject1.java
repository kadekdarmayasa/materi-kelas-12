public class GroupProject1 {
  // terdapat lima buah variabel string
  // masing - masing dari variabel menggunakan fungsi berikut 
  // toUpperCase, toLowerCase, substring, compareto, indexof

  public static void main(String[] args) {
    String description = "Belajar bahasa pemrograman itu sulit, tapi worth it.";

    // toUpperCase
    System.out.println(description.toUpperCase()); 

    // toLowerCase
    System.out.println(description.toLowerCase());

    // substring
    System.out.println(description.substring(42));

    // compareTo
    System.out.println(description.compareTo("Java merupakan salah satu jenis dari bahasa pemrograman yang ada"));

    // indexOF 
    System.out.println(description.indexOf("pemrograman"));
  }
}
