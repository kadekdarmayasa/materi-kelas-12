public class Task2 {
  public static void main(String[] args) {
    /** 
      length() => digunakan untuk mengetahui panjang string
    */
    // StringBuffer baru = new StringBuffer("StringBuffer");
    // System.out.println("\nBaru (capacity) : " + baru.capacity());
    // System.out.println("\nBaru (length) : " + baru.length());

    /** 
      concat() => digunakan untuk menggabungkan teks dari 1 strig dan string lainnya
    */
    // String halo = "Halo, ";
    // System.out.println(halo.concat("Linda").concat(" semoga harimu menyenangkan."));

    /** 
      delete() => digunakan untuk menghapus string pada indeks tertentu
    */
    // String kata = "StringBuffer";
    // StringBuffer baru2 = new StringBuffer(kata);
    // System.out.println("\nBaru (awal): " + baru2);
    // System.out.println("\nBaru (delete): " + baru2.delete(4, 8));


    /** 
      insert() => digunakan untuk menyisipkan string pada posisi tertentu
    */
    // String kata = "StringBuffer";
    // StringBuffer baru2 = new StringBuffer(kata);
    // System.out.println("\nBaru (insert): " + baru2.insert(6, " dan "));

    /** 
      append() => menambahkan string pada akhir string buffer
    */
    // String kata = "StringBuffer";
    // StringBuffer baru2 = new StringBuffer(kata);
    // System.out.println("\nBaru (append): " + baru2.append(" method append"));


   /** 
    setChartAt(index, char) => digunakan untuk mengubah karakter pada indeks tertentu
   */
    // String kata = "StrigBuffer";
    // StringBuffer baru2 = new StringBuffer(kata);
    
    // System.out.println("Baru awal:  " + baru2);
  
    // baru2.setCharAt(0, 's');
    // baru2.setCharAt(5, 'b');

    // System.out.println("Baru charAt1 : " + baru2);
   
    /** 
      Reverse() => metode ini digunakan untuk membalikkan isi string
    */
    // String kata = "StringBuffer";
    // StringBuffer kata_baru = new StringBuffer(kata);

    // System.out.println("Kata awal   : " + kata_baru);
    // System.out.println("Kata baru reverse() : " + kata_baru.reverse());


    /** 
      capacity () => metode ini digunakan untuk mengetahui kapasitas dari suatu varibel. Sederhananya metode ini akan menampilkan panjang dari sebuah string + 16.
    */ 
    // String kata = "hello";
    // StringBuffer a  = new StringBuffer();
    // StringBuffer b = new StringBuffer(kata);
    // System.out.println("a : " + a.capacity());
    // System.out.println("b : " + b.capacity() );
  }
}
