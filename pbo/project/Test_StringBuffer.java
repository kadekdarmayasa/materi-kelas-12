import java.lang.StringBuffer;

public class Test_StringBuffer {
  public static void main(String[] agrs) {
    StringBuffer stringBuffer = new StringBuffer("Belajar Java ");
    System.out.println(stringBuffer);
    System.out.println(); // empty line
    System.out.println("Sekarang string ditambah dengan " + "`Itu meyenangkan` menjadi:");
    System.out.println(stringBuffer.append("Itu menyenangkan"));
    System.out.println(); // empty line
    System.out.println("Sekarang `Memang ` dimasukkan " + "pada indeks 0 menjadi:");
    System.out.println(stringBuffer.insert(0, "Memang "));
    System.out.println(); // empty line
    System.out.println("Sekarang `Masa sih` menggantikan karakter pada " + "beginIndex 0 dan endIndex 6 menjadi:");
    System.out.println(stringBuffer.replace(0, 6, "Masa sih"));
    System.out.println(); // empty line
    System.out.println("Sekarang karakter dihapus pada " + "beginIndex 0 dan endIndex 9 menjadi:");
    System.out.println(stringBuffer.delete(0, 9));
  }
}
