import java.io.FileOutputStream;

public class modul {
  public static void main(String[] args) {
    try {
    FileOutputStream fos = new FileOutputStream("java.txt");
    String teks = "Saya belajar operasi file pada Java";
    byte[] isi = teks.getBytes();
    fos.write(isi);
    fos.close();
    } catch(Exception e) {
      System.err.println("Terjadi kesalahan saat menulis file");
    }
  }
}
